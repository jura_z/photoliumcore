//
//  GLProgram.h
//  PhotoliumCore
//
//
//
//

#import "GLProgram.h"
#include <iostream>
#include <algorithm>

#pragma mark Function Pointer Definitions

/// --------------------------------------------------------------------------------------------------------
/// --------------------------------------------------------------------------------------------------------
#pragma mark -

/// --------------------------------------------------------------------------------------------------------
GLProgram::GLProgram(const std::string &vShaderString, const std::string &fShaderString)
{
    initialized = false;

    program = glCreateProgram();
    
    if (!compileShader(&vertShader, GL_VERTEX_SHADER, vShaderString))
        std::cout << "Failed to compile vertex shader" << std::endl;
    
    if (!compileShader(&fragShader, GL_FRAGMENT_SHADER, fShaderString))
        std::cout << "Failed to compile fragment shader" << std::endl;
    
    glAttachShader(program, vertShader);
    glAttachShader(program, fragShader);
}

/// --------------------------------------------------------------------------------------------------------
bool GLProgram::compileShader(GLuint *shader, GLenum type, const std::string &shaderString)
{
    GLint status;
    const GLchar *source;
    
    source = (GLchar *)shaderString.c_str();

    if (!source)
    {
        NSLog(@"Failed to load vertex shader");
        return false;
    }
    
    *shader = glCreateShader(type);
    glShaderSource(*shader, 1, &source, nullptr);
    glCompileShader(*shader);
    
    glGetShaderiv(*shader, GL_COMPILE_STATUS, &status);

	if (status != GL_TRUE)
	{
		GLint logLength;
		glGetShaderiv(*shader, GL_INFO_LOG_LENGTH, &logLength);
		if (logLength > 0)
		{
			GLchar *log = (GLchar *)malloc(logLength);
			glGetShaderInfoLog(*shader, logLength, &logLength, log);
			NSLog(@"Shader compile log:\n%s", log);
			free(log);
		}
	}	
	
    return status == GL_TRUE;
}

#pragma mark -

/// --------------------------------------------------------------------------------------------------------
void GLProgram::addAttribute(const std::string &attributeName)
{
    auto it = find(attributes.begin(), attributes.end(), attributeName);
    
    if (it == attributes.end())
    {
        int attributeNameIndex = (int)attributes.size();
        attributes.push_back(attributeName);

        glBindAttribLocation(program, attributeNameIndex, attributeName.c_str());
    }
}

/// --------------------------------------------------------------------------------------------------------
int GLProgram::attributeIndex(const std::string &attributeName)
{
    auto it = find(attributes.begin(), attributes.end(), attributeName);
    
    if (it == attributes.end())
        return -1;
    
    size_t index = it - attributes.begin();

    return (int)index;
}

/// --------------------------------------------------------------------------------------------------------
int GLProgram::uniformIndex(const std::string &uniformName)
{
    return glGetUniformLocation(program, uniformName.c_str());
}

#pragma mark -

/// --------------------------------------------------------------------------------------------------------
bool GLProgram::link()
{
    if (initialized)
        NSLog(@"maybe double link");
    
    GLint status;
    
    glLinkProgram(program);
    
    glGetProgramiv(program, GL_LINK_STATUS, &status);
    if (status == GL_FALSE)
    {
        std::string progLog = programLog();
        std::cout << "Program link log: " << progLog << std::endl;
        
        std::string fragLog = fragmentShaderLog();
        std::cout << "Fragment shader compile log: " << fragLog << std::endl;

        std::string vertLog = vertexShaderLog();
        std::cout << "Vertex shader compile log log: " << vertLog << std::endl;
        return false;
    }
    
    if (vertShader)
    {
        glDeleteShader(vertShader);
        vertShader = 0;
    }
    if (fragShader)
    {
        glDeleteShader(fragShader);
        fragShader = 0;
    }
    
    initialized = true;
    return true;
}

/// --------------------------------------------------------------------------------------------------------
void GLProgram::use()
{
    glUseProgram(program);
}

#pragma mark -

/// --------------------------------------------------------------------------------------------------------
std::string GLProgram::logForOpenGLObject(GLuint object, GLInfoFunction infoFunc, GLLogFunction logFunc)
{
    GLint logLength = 0, charsWritten = 0;
    
    infoFunc(object, GL_INFO_LOG_LENGTH, &logLength);    
    if (logLength < 1)
        return nil;
    
    char *logBytes = (char *)malloc(logLength);
    logFunc(object, logLength, &charsWritten, logBytes);
    std::string log = logBytes;
    free(logBytes);
    return log;
}

/// --------------------------------------------------------------------------------------------------------
std::string GLProgram::vertexShaderLog()
{
    return logForOpenGLObject(vertShader, (GLInfoFunction)&glGetProgramiv, (GLLogFunction)&glGetProgramInfoLog);
    
}

/// --------------------------------------------------------------------------------------------------------
std::string GLProgram::fragmentShaderLog()
{
    return logForOpenGLObject(fragShader, (GLInfoFunction)&glGetProgramiv, (GLLogFunction)&glGetProgramInfoLog);
}

/// --------------------------------------------------------------------------------------------------------
std::string GLProgram::programLog()
{
    return logForOpenGLObject(program, (GLInfoFunction)&glGetProgramiv, (GLLogFunction)&glGetProgramInfoLog);
}


/// --------------------------------------------------------------------------------------------------------
void GLProgram::validate()
{
	GLint logLength;
	
	glValidateProgram(program);
	glGetProgramiv(program, GL_INFO_LOG_LENGTH, &logLength);
	if (logLength > 0)
	{
		GLchar *log = (GLchar *)malloc(logLength);
		glGetProgramInfoLog(program, logLength, &logLength, log);
		NSLog(@"Program validate log:\n%s", log);
		free(log);
	}	
}

#pragma mark -

/// --------------------------------------------------------------------------------------------------------
GLProgram::~GLProgram()
{
    if (vertShader)
        glDeleteShader(vertShader);
        
    if (fragShader)
        glDeleteShader(fragShader);
    
    if (program)
        glDeleteProgram(program);
}

