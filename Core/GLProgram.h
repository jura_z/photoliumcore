//
//  GLProgram.h
//  PhotoliumCore
//
//
//
//

#include <string>
#include <vector>

class GLProgram
{
    typedef void (*GLInfoFunction)(GLuint program, GLenum pname, GLint* params);
    typedef void (*GLLogFunction) (GLuint program, GLsizei bufsize, GLsizei* length, GLchar* infolog);
    
    bool compileShader(GLuint *shader, GLenum type, const std::string &shaderString);
    std::string logForOpenGLObject(GLuint object, GLInfoFunction infoFunc, GLLogFunction logFunc);
protected:
    
    std::vector<std::string> attributes;
    std::vector<std::string> uniforms;

    GLuint          program, vertShader, fragShader;
public:
    bool initialized;
public:
    void addAttribute(const std::string &attributeName);
    int attributeIndex(const std::string &attributeName);
    int uniformIndex(const std::string &uniformName);
    bool link();
    void use();
    std::string vertexShaderLog();
    std::string fragmentShaderLog();
    std::string programLog();
    void validate();
    
    GLProgram(const std::string &vShaderString, const std::string &fShaderString);
    ~GLProgram();
};

