//
//  WDBoxBlurFilter.m
//  PhotoliumCore
//
//  Created by Yurii Zakipnyi on 10/12/12.
//  Copyright (c) 2012 Yurii Zakipnyi. All rights reserved.
//

#include "WDBoxBlurFilter.h"
#include "GLProgram.h"

CGSize WDBoxBlurFilter::processWithSize()
{
    return CGSizeMake(640, 480);
}

bool WDBoxBlurFilter::setupShader()
{
    /// --------------------------------------------------------------------------------------------------------
    NSString *const VS_BoxBlur = SHADER_STRING
    (
     attribute vec4 inputTextureCoordinate;
     attribute vec4 position;
     uniform mediump vec2 texelSize;
     
     varying vec2 textureCoordinateA;
     varying vec2 textureCoordinateB;
     varying vec2 textureCoordinateC;
     varying vec2 textureCoordinateD;
     
     void main()
     {
         gl_Position = position;
         textureCoordinateA = inputTextureCoordinate.xy + vec2( texelSize.x * 1.5,  texelSize.y * 1.5);
         textureCoordinateB = inputTextureCoordinate.xy + vec2( texelSize.x * 1.5, -texelSize.y * 1.5);
         textureCoordinateC = inputTextureCoordinate.xy + vec2(-texelSize.x * 1.5,  texelSize.y * 1.5);
         textureCoordinateD = inputTextureCoordinate.xy + vec2(-texelSize.x * 1.5, -texelSize.y * 1.5);
     }
     );
    
    /// --------------------------------------------------------------------------------------------------------
    NSString *const FS_BoxBlur = SHADER_STRING
    (
     varying highp vec2 textureCoordinateA;
     varying highp vec2 textureCoordinateB;
     varying highp vec2 textureCoordinateC;
     varying highp vec2 textureCoordinateD;

     uniform sampler2D inputImageTexture;
     
     void main()
     {
         lowp vec4 color = vec4(0);
         color += texture2D(inputImageTexture, textureCoordinateA) * 0.25;
         color += texture2D(inputImageTexture, textureCoordinateB) * 0.25;
         color += texture2D(inputImageTexture, textureCoordinateC) * 0.25;
         color += texture2D(inputImageTexture, textureCoordinateD) * 0.25;
         gl_FragColor = color;
     }
     );
    /// --------------------------------------------------------------------------------------------------------
    
    
    defProgram = new GLProgram([VS_BoxBlur UTF8String], [FS_BoxBlur UTF8String]);
    
    if (!defProgram->initialized)
    {
        defProgram->addAttribute("inputTextureCoordinate");
        defProgram->addAttribute("position");
        
        if (!defProgram->link())
        {
            assert(false);//NSAssert(false, @"Filter shader link failed");
            return false;
        }

        this->addInput("inputImageTexture");
        uniTexSize = defProgram->uniformIndex("texelSize");
    }
    return true;
}

void WDBoxBlurFilter::prerenderSetup()
{
    static bool inited = false;
    if (inited == false)
    {
        CGSize s = processWithSize();
        glUniform2f(uniTexSize, 1.0f / s.width, 1.0f / s.height);
    }
    inited = true;
}
