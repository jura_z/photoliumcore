//
//  WDBoxBlurFilter.h
//  PhotoliumCore
//
//  Created by Yurii Zakipnyi on 10/12/12.
//  Copyright (c) 2012 Yurii Zakipnyi. All rights reserved.
//

#include "WDFilter.h"

class WDBoxBlurFilter : public WDFilter
{
protected:
    int uniTexSize;
protected:
    virtual void prerenderSetup();
public:
    virtual bool setupShader();
    CGSize processWithSize();
};
