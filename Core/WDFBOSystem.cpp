//
//  WDFBOSystem.m
//  PhotoliumCore
//
//  Created by Yurii Zakipnyi on 10/12/12.
//  Copyright (c) 2012 Yurii Zakipnyi. All rights reserved.
//

#import "WDFBOSystem.h"
#import "WDFBObject.h"

#include <sstream>

/// --------------------------------------------------------------------------------------------------------
WDFBOSystem *WDFBOSystem::sharedFBOSystem()
{
    static WDFBOSystem *instance = nullptr;

    static std::once_flag flag;
    std::call_once(flag, [](){ instance = new WDFBOSystem(); });
    return instance;
}

/// --------------------------------------------------------------------------------------------------------
std::string WDFBOSystem::keyWithSize(CGSize size)
{
    char buffer[64];
    sprintf (buffer, "%d_%d", (int)size.width, (int)size.height);
    return &buffer[0];
}

/// --------------------------------------------------------------------------------------------------------
WDFBObject *WDFBOSystem::createFBOWithSize(CGSize size)
{
    WDFBObject *fbo = new WDFBObject();
    fbo->attachTextureWithSize(size);
    
    std::string key = keyWithSize(size);
    std::vector<WDFBObject *> &arr = fboDict[key];
    
    arr.push_back(fbo);

    return fbo;
}

/// --------------------------------------------------------------------------------------------------------
WDFBObject *WDFBOSystem::findNotUsedFBOWithSize(CGSize size)
{
    std::string key = keyWithSize(size);
    
    auto arrIter = fboDict.find(key);
    if (arrIter == fboDict.end())
        return nullptr;
    
    std::vector<WDFBObject *> fboArray = arrIter->second;
    
    for (auto fbo : fboArray)
    {
        if (fbo->useCount == 0)
            return fbo;
    }
    
    return nullptr;
}

/// --------------------------------------------------------------------------------------------------------
WDFBObject *WDFBOSystem::retainFBOWithSize(CGSize size)
{
    WDFBObject *fbo = findNotUsedFBOWithSize(size);
    
    if (!fbo)
    {
        fbo = createFBOWithSize(size);
    }
    return fbo;
}

/// --------------------------------------------------------------------------------------------------------
void WDFBOSystem::releaseFBO(WDFBObject *fbo)
{
//    fbo.useCount == 0;
}


#pragma mark - ctor/dtor

/// --------------------------------------------------------------------------------------------------------
WDFBOSystem::WDFBOSystem()
{
}

/// --------------------------------------------------------------------------------------------------------
WDFBOSystem::~WDFBOSystem()
{
}

