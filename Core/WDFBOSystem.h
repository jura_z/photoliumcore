//
//  WDFBOSystem.h
//  PhotoliumCore
//
//  Created by Yurii Zakipnyi on 10/12/12.
//  Copyright (c) 2012 Yurii Zakipnyi. All rights reserved.
//

#include <map>
#include <vector>
#include <string>

class WDFBObject;

class WDFBOSystem
{
    std::map<std::string, std::vector<WDFBObject*>> fboDict;

protected:
    std::string keyWithSize(CGSize size);
    WDFBObject *createFBOWithSize(CGSize size);
    WDFBObject *findNotUsedFBOWithSize(CGSize size);

public:
    static WDFBOSystem *sharedFBOSystem();
    
    WDFBObject *retainFBOWithSize(CGSize size);
    void releaseFBO(WDFBObject *fbo);
    
    WDFBOSystem();
    ~WDFBOSystem();
};
