//
//  WDFBObject.m
//  PhotoliumCore
//
//  Created by Yurii Zakipnyi on 10/10/12.
//  Copyright (c) 2012 Yurii Zakipnyi. All rights reserved.
//

#import "WDFBObject.h"
#import <QuartzCore/QuartzCore.h>
#include "WDTexture.h"

#include "WDOpenGLSystem.h"

#pragma mark - FBO

/// --------------------------------------------------------------------------------------------------------
void WDFBObject::checkFBO()
{
    GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    if (status != GL_FRAMEBUFFER_COMPLETE)
    {
        NSLog(@"Incomplete filter FBO: %d", status);
        assert(false);//NSAssert(false, @"Incomplete filter FBO: %d", status);
    }
}

/// --------------------------------------------------------------------------------------------------------
void WDFBObject::createFBO()
{
    if (fboId)
    {
        NSLog(@"ERROR [FBO] FBO already created");
        return;
    }
    
    fboId = WDOpenGLSystem::sharedOpenGLSystem()->fboManager.createFBO();
    
    inited = true;
    NSLog(@"[FBO] FBO created");
}
    
/// --------------------------------------------------------------------------------------------------------
void WDFBObject::deleteFBO()
{
    if (fboId)
    {
        WDOpenGLSystem::sharedOpenGLSystem()->fboManager.deleteFBO(fboId);
        fboId = 0;
        NSLog(@"GeneralRendertarget: def framebuffer deleted");
    }
    
    if (colorRenderbuffer)
    {
        WDOpenGLSystem::sharedOpenGLSystem()->fboManager.deleteRenderbuffer(colorRenderbuffer);
        colorRenderbuffer = 0;
        NSLog(@"GeneralRendertarget: color renderbuffer deleted");
    }
    
    inited = false;
    NSLog(@"[FBO] FBO deleted");
}

/// --------------------------------------------------------------------------------------------------------
void WDFBObject::attachGeneralRendertarget(CAEAGLLayer *layer, EAGLContext *context)
{
    WDOpenGLSystem::sharedOpenGLSystem()->fboManager.bindFBO(fboId);
    
    useCount = -99;

    colorRenderbuffer = WDOpenGLSystem::sharedOpenGLSystem()->fboManager.createRenderbuffer();
    WDOpenGLSystem::sharedOpenGLSystem()->fboManager.bindRenderbuffer(colorRenderbuffer);
    
    [context renderbufferStorage:GL_RENDERBUFFER fromDrawable:layer];
    
    GLint width, height;
    WDOpenGLSystem::sharedOpenGLSystem()->fboManager.getCurrentRenderbufferSize(&width, &height);
    WDOpenGLSystem::sharedOpenGLSystem()->fboManager.attachRenderbuffer(colorRenderbuffer);

    size = CGSizeMake(width, height);
}

void WDFBObject::bindRenderbuffer()
{
    WDOpenGLSystem::sharedOpenGLSystem()->fboManager.bindRenderbuffer(colorRenderbuffer);
}

/// --------------------------------------------------------------------------------------------------------
WDTexture *WDFBObject::attachTextureWithSize(CGSize s)
{
    if (texture != nullptr)
    {
        NSLog(@"ERROR: already have texture attached!");
        return texture;
    }
    
    size = s;
    
    texture = new WDTexture(s);

    WDOpenGLSystem::sharedOpenGLSystem()->fboManager.bindFBO(fboId);
    WDOpenGLSystem::sharedOpenGLSystem()->fboManager.attachTexture(texture);

    return texture;
}

/// --------------------------------------------------------------------------------------------------------
void WDFBObject::bindFBO()
{
    WDOpenGLSystem::sharedOpenGLSystem()->textureManager.unbindTexture();
    WDOpenGLSystem::sharedOpenGLSystem()->fboManager.bindFBO(fboId);
    WDOpenGLSystem::sharedOpenGLSystem()->setViewport((int)size.width, (int)size.height);
    
    checkFBO();
}

WDFBObject::WDFBObject()
{
    fboId = 0;
    inited = false;
    colorRenderbuffer = 0;
    texture = nullptr;
    size = CGSizeZero;
    useCount = 0;
    
    createFBO();
}

WDFBObject::~WDFBObject()
{
    if (texture)
    {
        delete texture;
        texture = nullptr;
    }
    deleteFBO();
}

