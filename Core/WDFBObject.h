//
//  WDFBObject.h
//  PhotoliumCore
//
//  Created by Yurii Zakipnyi on 10/10/12.
//  Copyright (c) 2012 Yurii Zakipnyi. All rights reserved.
//

@class CAEAGLLayer;

class WDTexture;

class WDFBObject
{
    GLuint          fboId = 0;
    bool            inited = false;
    GLuint          colorRenderbuffer = 0;
protected:
    void checkFBO();
    void createFBO();
    void deleteFBO();
public:
    WDTexture*      texture = nullptr;
    CGSize          size = CGSizeZero;
    int             useCount = 0;
public:
    GLuint getId() const { return fboId; }
    
    void attachGeneralRendertarget(CAEAGLLayer *layer, EAGLContext *context);

    void bindFBO();
    void bindRenderbuffer();
    WDTexture *attachTextureWithSize(CGSize size);
    
    WDFBObject();
    ~WDFBObject();
};
