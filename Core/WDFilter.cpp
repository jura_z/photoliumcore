//
//  WDFilter.m
//  PhotoliumCore
//
//  Created by Yurii Zakipnyi on 10/8/12.
//  Copyright (c) 2012 Yurii Zakipnyi. All rights reserved.
//

#import "WDFilter.h"
#import "GLProgram.h"
#import "WDVertexData.h"
#import "WDTexture.h"
#import "WDTextureImage.h"
#import "WDFBObject.h"
#import "WDFBOSystem.h"
//#import "WDCamera.h"

#import "WDFilterSystem.h"

#include "WDOpenGLSystem.h"

/// --------------------------------------------------------------------------------------------------------
void WDFilterInputObject::bind()
{
    slotToUse = WDOpenGLSystem::sharedOpenGLSystem()->textureManager.getFreeTextureSlot();

    WDTexture *tex = (filter) ? filter->fboRes()->texture : texture;
    
    if (tex)
    {
        tex->bind(slotToUse);
        glUniform1i(uniformLocation, slotToUse);
    }
}

/// --------------------------------------------------------------------------------------------------------
void WDFilterInputObject::unbind()
{
    WDOpenGLSystem::sharedOpenGLSystem()->textureManager.unbindTextureInSlot(slotToUse);
    
    if (filter)
    {
        filter->onDidUse();
    }
}

/// --------------------------------------------------------------------------------------------------------
/// WDFilter
/// --------------------------------------------------------------------------------------------------------

/// --------------------------------------------------------------------------------------------------------
CGSize WDFilter::processWithSize()
{
    return CGSizeMake(640, 480);
}

bool WDFilter::isLeaf()
{
    return targetsCount == -1;
}

bool WDFilter::setupShader()
{
    /// --------------------------------------------------------------------------------------------------------
    NSString *const VS_Default = SHADER_STRING
    (
     attribute vec4 inputTextureCoordinate;
     attribute vec4 position;
     
     varying vec2 textureCoordinate;
     
     void main()
     {
         gl_Position = position;
         textureCoordinate = inputTextureCoordinate.xy;
     }
     );
    
    /// --------------------------------------------------------------------------------------------------------
    NSString *const FS_Default = SHADER_STRING
    (
     varying highp vec2 textureCoordinate;
     
     uniform sampler2D inputImageTexture;
     
     void main()
     {
         gl_FragColor = texture2D(inputImageTexture, textureCoordinate);
     }
     );
    /// --------------------------------------------------------------------------------------------------------
    
    defProgram = new GLProgram([VS_Default UTF8String], [FS_Default UTF8String]);
    
    if (!defProgram->initialized)
    {
        defProgram->addAttribute("inputTextureCoordinate");
        defProgram->addAttribute("position");
        
        if (!defProgram->link())
        {
            assert(false);//NSAssert(false, @"Filter shader link failed");
            return false;
        }
        
        addInput("inputImageTexture");
    }

    return true;
}

void WDFilter::prerenderSetup()
{
    
}


void WDFilter::process()
{
    if (targetsCount == 0)
    {
//        NSLog(@"targetCount == 0, so result of this is not useful");
        return;
    }
   // NSLog(@" =begin= ");
    
    if (targetsCount > 0)
    {
        fbo = WDFBOSystem::sharedFBOSystem()->retainFBOWithSize(processWithSize());
        fbo->useCount = targetsCount;
    //    NSLog(@"%@ target counts = %d", self, targetsCount);
    //    NSLog(@"%d - for FBO", fboRes.texture.Id);
    }
    //else
    //    NSLog(@" - ");
    
    
    fbo->bindFBO();
    
    if (fbo->texture)
        fbo->texture->unbind();
    
    GLuint fboTexId = (fbo->texture) ? fbo->texture->getId() : 0;
    
    glClear(GL_COLOR_BUFFER_BIT);
    
    defProgram->use();
    BOOL hasCameraAsInput = false;
    for (auto pair : inputs)
    {
        WDFilterInputObject *inObject = pair.second;
        if (inObject->texture)
        {
            if (fboTexId == inObject->texture->getId())
                NSLog(@"ERROR! We are rendering one FBO to the same FBO. Undefined behavior!");
      
//            if ([inObject.texture isKindOfClass:[WDCamera class]])
//            {
//                hasCameraAsInput = true;
//            }
        }
        inObject->bind();
    }

    if (hasCameraAsInput)
        vdata = WDVertexData::fullScreenCamera();
    else
        vdata = WDVertexData::fullScreen();
    
    prerenderSetup();

    int pos = defProgram->attributeIndex("position");
    int texCoord = defProgram->attributeIndex("inputTextureCoordinate");
    
    if (WDOpenGLSystem::sharedOpenGLSystem()->textureManager.currentTextureSlot() == 0)
    {
        NSLog(@"ERROR! texture not binded");
    }
    
    vdata->draw(pos, texCoord);

    for (auto pair : inputs)
    {
        WDFilterInputObject *inObject = pair.second;
        inObject->unbind();
    }
    WDOpenGLSystem::sharedOpenGLSystem()->textureManager.unbindTexture();
}

#pragma mark -

/// --------------------------------------------------------------------------------------------------------
WDFilterInputObject *WDFilter::addInput(const std::string &name)
{
    WDFilterInputObject *inObject = inputs[name];
    if (inObject)
        return inObject;
    
    int uniLocation = defProgram->uniformIndex(name);
    if (uniLocation < 0)
    {
        NSLog(@"[add] %s not found", name.c_str());
        assert(false);
        return nullptr;
    }
    
    inObject = new WDFilterInputObject();
    inObject->uniformLocation = uniLocation;
    inputs[name] = inObject;

    return inObject;
}

/// --------------------------------------------------------------------------------------------------------
void WDFilter::connectTexture(WDTexture *tex, const std::string &name)
{
    WDFilterInputObject *inObject = addInput(name);
    inObject->texture = tex;
    inObject->filter = nil;
}

/// --------------------------------------------------------------------------------------------------------
void WDFilter::connectFilter(WDFilter *filter, const std::string &name)
{
    filter->addTarget(this, name);
}

/// --------------------------------------------------------------------------------------------------------
void WDFilter::removeTraget(WDFilter *filter, const std::string &name)
{
    WDFilterInputObject *inObject = filter->inputs[name];
    if (inObject && inObject->filter == this)
    {
        filter->inputs.erase(name);
    }
}

/// --------------------------------------------------------------------------------------------------------
void WDFilter::addTarget(WDFilter *filter, const std::string &name)
{
    WDFilterInputObject *inObject = filter->inputs[name];
    if (inObject && inObject->filter)
    {
        if (inObject->filter == this) return;
    }
    else
        inObject = filter->addInput(name);

    inObject->filter = this;
}

/// --------------------------------------------------------------------------------------------------------
void WDFilter::onDidUse()
{
    fbo->useCount = fbo->useCount - 1;
    //NSLog(@"%@ used (%d)", self, fboRes.useCount);
    if (fbo->useCount == 0)
    {
        WDFBOSystem::sharedFBOSystem()->releaseFBO(fbo);
        fbo = nullptr;
    }
}

/// --------------------------------------------------------------------------------------------------------
void WDFilter::for_each_parent_filter(std::function<void (WDFilter *)> func)
{
    for (auto pairInObject : inputs)
    {
        WDFilterInputObject *inObject = pairInObject.second;
        if (inObject->filter == nil) continue; // skip not-filter parents

        func(inObject->filter);
    }
}

/// --------------------------------------------------------------------------------------------------------
WDFilter::WDFilter()
{
    if (vdata == nil)
        vdata = WDVertexData::fullScreen();
    
    WDFilterSystem::sharedFilters()->addFilter(this);
}

/// --------------------------------------------------------------------------------------------------------
WDFilter::~WDFilter()
{
    if (fbo)
        delete fbo;
    if (defProgram)
        delete defProgram;
}

