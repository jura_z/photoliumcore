//
//  WDFilter.h
//  PhotoliumCore
//
//  Created by Yurii Zakipnyi on 10/8/12.
//  Copyright (c) 2012 Yurii Zakipnyi. All rights reserved.
//

#include <map>
#include <string>

class GLProgram;
class WDVertexData;
class WDFBObject;
class WDTexture;
class WDFilter;

struct WDFilterInputObject
{
    int         uniformLocation = 0;
    WDTexture*  texture = nullptr;
    WDFilter*   filter = nullptr;
    int         slotToUse = 0;
    
    void bind();
    void unbind();
};

class WDFilter
{
    friend class WDFilterSystem;
protected:
    GLProgram*          defProgram = nullptr;
    WDVertexData*       vdata = nullptr;
    WDFBObject*         fbo = nullptr;
    int                 targetsCount = 0;
protected:
    virtual void prerenderSetup();

    WDFilterInputObject *addInput(const std::string &name);
public:
    std::map<std::string, WDFilterInputObject *> inputs;
public:
    WDFilter();
    virtual ~WDFilter();

    virtual bool setupShader();

    virtual CGSize processWithSize();

    bool isLeaf();

    void onDidUse();
    
    int getTargetsCount() const { return targetsCount; }

    void for_each_parent_filter(std::function<void (WDFilter *)> func);
    
    WDFBObject *fboRes() const { return fbo; }
    virtual void process();
    void removeTraget(WDFilter *filter, const std::string &name);
    void addTarget(WDFilter *filter, const std::string &name);
    void connectTexture(WDTexture *tex, const std::string &name);
    void connectFilter(WDFilter *filter, const std::string &name);
};
