//
//  WDFilterSystem.m
//  PhotoliumCore
//
//  Created by Yurii Zakipnyi on 10/12/12.
//  Copyright (c) 2012 Yurii Zakipnyi. All rights reserved.
//

#import "WDFilterSystem.h"
#import "WDFilter.h"

/// --------------------------------------------------------------------------------------------------------
WDFilterSystem *WDFilterSystem::sharedFilters()
{
    static WDFilterSystem *instance = nullptr;
    
    static std::once_flag flag;
    std::call_once(flag, [](){ instance = new WDFilterSystem(); });
    return instance;

}

/// --------------------------------------------------------------------------------------------------------
void WDFilterSystem::addFilter(WDFilter *filter)
{
    filters.push_back(filter);
    reconfigure();
}


/// --------------------------------------------------------------------------------------------------------
/// returns only useful filters, that are connected to the main flow
/// --------------------------------------------------------------------------------------------------------
std::vector<WDFilter *> WDFilterSystem::getFiltersToWorkWith()
{
    std::vector<WDFilter *> result;
    result.reserve(filters.size());

    std::vector<WDFilter *> queue;
    queue.reserve(filters.size());
    
    for (WDFilter *f : filters)
    {
        if (f->isLeaf())
            queue.push_back(f);
    }
    
    while (!queue.empty())
    {
        WDFilter *f = queue.back();
        queue.pop_back();

        result.push_back(f);
        
        for (auto pairInObject : f->inputs)
        {
            WDFilterInputObject *inObject = pairInObject.second;
            if (inObject->filter)
                queue.push_back(inObject->filter);
        }
    }
    
    return result;
}

/// --------------------------------------------------------------------------------------------------------
int WDFilterSystem::getIndexInWorkingArrayFor(WDFilter *filter)
{
    auto pIterator = find(filtersToWork.begin(), filtersToWork.end(), filter);
    if (pIterator == filtersToWork.end()) return -1;
    return (int)(pIterator - filtersToWork.begin());
}

/// --------------------------------------------------------------------------------------------------------
std::list<int> WDFilterSystem::candidatesForOrder(const std::vector<int> &order)
{
    std::list<int> result;
    
    int i = 0;
    for (int num : order)
    {
        if (num != -1) continue;    // skip processed
        
        WDFilter *filter = filtersToWork[i];
        
        // if all parents are processed - add to result
        bool allParentsAreProcessed = true;

        filter->for_each_parent_filter([this, &order, &allParentsAreProcessed] (WDFilter *parentFilter) {
            
            if (allParentsAreProcessed == false) return;
            
            int pIndx = getIndexInWorkingArrayFor(parentFilter);
            if (pIndx < 0) return; // skip not-working filters
            
            BOOL pNotReady = order[pIndx] == -1;
            if (pNotReady)
            {
                allParentsAreProcessed = false;
            }
        });
        
        if (allParentsAreProcessed)
            result.push_back(i);
        
        ++i;
    }
    
    return result;
}

/// --------------------------------------------------------------------------------------------------------
/// returns optimal filter processing order
/// --------------------------------------------------------------------------------------------------------
std::vector<int> WDFilterSystem::getOptimalOrder()
{
    int n = (int)filtersToWork.size();

    struct brutforceState
    {
        int proceeded;
        int memory;
        std::vector<int> order;
        std::vector<int> usage;
    };

    std::vector<brutforceState> queue;
    queue.reserve(32);

    {
        brutforceState initState = {0, 0, std::vector<int>(n, -1), std::vector<int>(n, 0)};
        queue.push_back( initState );
    }
    
    int currentBestScore = INT32_MAX;
    std::vector<int> currentBestResult;
    
    while (queue.size() > 0)
    {
        brutforceState state = queue.back();
        queue.pop_back();

        int memory = state.memory;
        
        // get candidates for current state
        std::list<int> candidates = candidatesForOrder(state.order);

        if (candidates.size() == 0)
        {
            if (currentBestScore > state.memory)
            {
                currentBestScore = state.memory;
                currentBestResult = state.order;
            }
        }
        else
        {
            for (int cIndex : candidates)
            {
                auto newStateArray = state.order;
                auto newUsageArray = state.usage;

                newStateArray[cIndex] = state.proceeded;
                
                // simulate "process" method. FBO's useCount
                {
                    // memory alloc
                    WDFilter *filter = filtersToWork[cIndex];
                    CGSize filterSize = filter->processWithSize();
                    
                    memory += filterSize.width * filterSize.height;
     
                    // set use counter
                    newUsageArray[cIndex] = filter->getTargetsCount();
                    
                    CGSize pFilterSize = filter->processWithSize();
                    filter->for_each_parent_filter([this, &newUsageArray, &memory, &pFilterSize] (WDFilter *parentFilter) {
                        
                        int pIndx = getIndexInWorkingArrayFor(parentFilter);
                        if (pIndx < 0) return; // skip not-working filters
                        
                        int pUsage = newUsageArray[pIndx] - 1;
                        newUsageArray[pIndx] = pUsage;
                        
                        // release memory
                        if (pUsage == 0)
                            memory -= pFilterSize.width * pFilterSize.height;
                    });
                }
                
                if (memory < currentBestScore)
                {
                    brutforceState newState =  {state.proceeded + 1, memory, newStateArray, newUsageArray};
                    queue.push_back(newState);
                }
            }
        }
    }

    return currentBestResult;
}

/// --------------------------------------------------------------------------------------------------------
void WDFilterSystem::setActualTargetCount()
{
    for (WDFilter *filter : filtersToWork)
    {
        if (filter->targetsCount < 0) continue;
        filter->targetsCount = 0;
    }
    
    for (WDFilter *filter : filtersToWork)
    {
        filter->for_each_parent_filter([this] (WDFilter *parentFilter) {
            if (parentFilter->targetsCount < 0) return;
            
            int pIndx = getIndexInWorkingArrayFor(parentFilter);
            if (pIndx < 0) return; // skip not-working filters
            
            parentFilter->targetsCount++;
        });
    }
}

/// --------------------------------------------------------------------------------------------------------
void WDFilterSystem::process()
{
    if (orderToProcess.empty() || filtersToWork.empty())
    {
        filtersToWork = getFiltersToWorkWith();
        orderToProcess = getOptimalOrder();
    }

    setActualTargetCount();

    for (int filterIndex : orderToProcess)
    {
        WDFilter *filter = filtersToWork[filterIndex];
        filter->process();
    }
}

/// --------------------------------------------------------------------------------------------------------
void WDFilterSystem::reconfigure()
{
    orderToProcess = {};
}

/// --------------------------------------------------------------------------------------------------------
WDFilterSystem::WDFilterSystem()
{
    reconfigure();
}
