//
//  WDFilterSystem.h
//  PhotoliumCore
//
//  Created by Yurii Zakipnyi on 10/12/12.
//  Copyright (c) 2012 Yurii Zakipnyi. All rights reserved.
//

class WDFilter;

class WDFilterSystem
{
    std::vector<WDFilter *>     filters;
    std::vector<int>            orderToProcess;
    std::vector<WDFilter *>     filtersToWork;
    
protected:
    int getIndexInWorkingArrayFor(WDFilter *filter);
    std::vector<int> getOptimalOrder();
    std::list<int> candidatesForOrder(const std::vector<int> &order);
    std::vector<WDFilter *> getFiltersToWorkWith();
    void setActualTargetCount();

    WDFilterSystem();
public:
    static WDFilterSystem *sharedFilters();
    
    void addFilter(WDFilter *filter);
    void reconfigure();
    void process();
};
