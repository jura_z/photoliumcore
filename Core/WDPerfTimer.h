//
//  TRPerfTimer.h
//  Teora Reader
//
//  Created by Yurii Zakipnyi on 7/12/12.
//  Copyright (c) 2012 WD. All rights reserved.
//

@interface WDPerfTimer : NSObject

+ (WDPerfTimer *)perfTimer;
+ (WDPerfTimer *)perfTimerWithName:(NSString *)n;

- (long long)deltaTime;
- (long long)passedTime;
- (void)printElapsedTime;
- (void)printElapsedTimeAndResetWithMessage:(NSString *)msg;

- (float)deltaMemKB;
- (float)memoryUsedKB;
- (void)printMemUsage;
- (void)printMemUsageAndResetWithMessage:(NSString *)msg;

@end
