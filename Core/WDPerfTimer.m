//
//  TRPerfTimer.m
//  Teora Reader
//
//  Created by Yurii Zakipnyi on 7/12/12.
//  Copyright (c) 2012 Teora. All rights reserved.
//

#import "WDPerfTimer.h"
#import "WDProfiler.h"

typedef std::chrono::high_resolution_clock::time_point _time_t;

@implementation WDPerfTimer
{
    
    NSString    *name;
    _time_t     date;
    _time_t     lastDate;
    
    long        mem;
    long        lastMem;
}



+ (WDPerfTimer *)perfTimer
{
    return [WDPerfTimer perfTimerWithName:@""];
}

+ (WDPerfTimer *)perfTimerWithName:(NSString *)n
{
    WDPerfTimer *p = [[WDPerfTimer alloc] init];
    p->name = n;
    
    
    p->date = std::chrono::high_resolution_clock::now();
    p->mem = [[WDProfiler sharedProfiler] usedMemory];
    
    p->lastDate = p->date;
    p->lastMem = p->mem;
    return p;
}

- (long long)deltaTime
{
    auto t2 = std::chrono::high_resolution_clock::now();
    auto nano = std::chrono::duration_cast<std::chrono::nanoseconds>(t2-lastDate).count();
    self->lastDate = std::chrono::high_resolution_clock::now();

    return nano;
}

- (float)deltaMemKB
{
    float dt = ([[WDProfiler sharedProfiler] usedMemory] - self->lastMem) / 1000.0f;
    self->lastMem = [[WDProfiler sharedProfiler] usedMemory];
    return dt;
}

- (long long)passedTime
{
    auto t2 = std::chrono::high_resolution_clock::now();
    auto nano = std::chrono::duration_cast<std::chrono::nanoseconds>(t2-lastDate).count();

    return nano;
}

- (float)memoryUsedKB
{
    return ([[WDProfiler sharedProfiler] usedMemory] - self->mem) / 1000.0f;
}

- (void)printElapsedTime
{
    NSLog(@"[%@]%lld nanosec", name, [self passedTime]);
}

- (void)printElapsedTimeAndResetWithMessage:(NSString *)msg
{
    NSLog(@"[%@]%@: %lld nanosec", name, msg, [self deltaTime]);
}

- (void)printMemUsage
{
    NSLog(@"[%@]%f KB, used:%f KB", name, [self memoryUsedKB], [[WDProfiler sharedProfiler] usedMemory] / 1000.0f);
}

- (void)printMemUsageAndResetWithMessage:(NSString *)msg
{
    NSLog(@"[%@]%@: %f KB", name, msg, [self deltaMemKB]);
}

@end
