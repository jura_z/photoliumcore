//
//  WDProfiler.h
//  PhotoliumCore
//
//  Created by Yurii Zakipnyi on 10/13/12.
//  Copyright (c) 2012 Yurii Zakipnyi. All rights reserved.
//

@interface WDProfiler : NSObject

+ (WDProfiler *)sharedProfiler;

- (vm_size_t)usedMemory;
- (vm_size_t)freeMemory;
- (void)logMemUsage;

- (void)addFBO:(CGSize)size;
- (void)removeFBO:(CGSize)size;

- (void)printStatus;

@end
