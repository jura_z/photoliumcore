//
//  WDProfiler.m
//  PhotoliumCore
//
//  Created by Yurii Zakipnyi on 10/13/12.
//  Copyright (c) 2012 Yurii Zakipnyi. All rights reserved.
//

#import "WDProfiler.h"
#import "mach/mach.h"

@implementation WDProfiler
{
    NSUInteger FBOMemoryUsed;
    NSInteger FBOCount;
}

- (vm_size_t)usedMemory
{
    struct task_basic_info info;
    mach_msg_type_number_t size = sizeof(info);
    kern_return_t kerr = task_info(mach_task_self(), TASK_BASIC_INFO, (task_info_t)&info, &size);
    return (kerr == KERN_SUCCESS) ? info.resident_size : 0; // size in bytes
}

- (vm_size_t)freeMemory
{
    mach_port_t host_port = mach_host_self();
    mach_msg_type_number_t host_size = sizeof(vm_statistics_data_t) / sizeof(integer_t);
    vm_size_t pagesize;
    vm_statistics_data_t vm_stat;
    
    host_page_size(host_port, &pagesize);
    (void) host_statistics(host_port, HOST_VM_INFO, (host_info_t)&vm_stat, &host_size);
    return vm_stat.free_count * pagesize;
}

- (void)logMemUsage
{
    // compute memory usage and log if different by >= 100k
    static long prevMemUsage = 0;
    long curMemUsage = [self usedMemory];
    long memUsageDiff = curMemUsage - prevMemUsage;
    
    if (memUsageDiff > 100000 || memUsageDiff < -100000)
    {
        prevMemUsage = curMemUsage;
        NSLog(@"Memory used %7.1f (%+5.0f), free %7.1f kb", curMemUsage/1000.0f, memUsageDiff/1000.0f, [self freeMemory]/1000.0f);
    }
}




+ (WDProfiler *)sharedProfiler
{
    static WDProfiler *instance = nullptr;
    
    static std::once_flag flag;
    std::call_once(flag, [](){ instance = [[WDProfiler alloc] init]; });
    return instance;
}

- (void)addFBO:(CGSize)size
{
    ++FBOCount;
    FBOMemoryUsed += size.width * size.height * 4;
    [self printStatus];
}

- (void)removeFBO:(CGSize)size
{
    --FBOCount;
    FBOMemoryUsed -= size.width * size.height * 4;
    [self printStatus];
}



- (void)printStatus
{
    NSLog(@"FBOMemoryUsed: %7.1f kb (%ld)", FBOMemoryUsed / 1000.0f, (long)FBOCount);
    [self logMemUsage];
}

@end
