//
//  WDResult.m
//  PhotoliumCore
//
//  Created by Yurii Zakipnyi on 10/12/12.
//  Copyright (c) 2012 Yurii Zakipnyi. All rights reserved.
//

#import "WDResult.h"
#import "WDFBObject.h"
#import "GLProgram.h"
#import "WDVertexData.h"
#import "WDTexture.h"

/// --------------------------------------------------------------------------------------------------------
void WDResult::process()
{
    WDFilter::process();
    fbo->bindRenderbuffer();
}

/// --------------------------------------------------------------------------------------------------------
WDResult::WDResult(WDFBObject* defFBO) : WDFilter()
{
    fbo = defFBO;
    targetsCount = -1;
}
