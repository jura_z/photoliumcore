//
//  WDResult.h
//  PhotoliumCore
//
//  Created by Yurii Zakipnyi on 10/12/12.
//  Copyright (c) 2012 Yurii Zakipnyi. All rights reserved.
//

#import "WDFilter.h"

class WDFBObject;

class WDResult : public WDFilter
{
    
public:
    WDResult(WDFBObject * defFBO);
    void process();
};
