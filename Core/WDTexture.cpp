//
//  WDTexture.m
//  PhotoliumCore
//
//  Created by Yurii Zakipnyi on 10/9/12.
//  Copyright (c) 2012 Yurii Zakipnyi. All rights reserved.
//

#import "WDTexture.h"

#include "WDOpenGLSystem.h"

/// --------------------------------------------------------------------------------------------------------
void WDTexture::bind()
{
    WDOpenGLSystem::sharedOpenGLSystem()->textureManager.bindTexture(Id);
}

/// --------------------------------------------------------------------------------------------------------
void WDTexture::bind(int indx)
{
    WDOpenGLSystem::sharedOpenGLSystem()->textureManager.bindTexture(Id, indx);
}

/// --------------------------------------------------------------------------------------------------------
void WDTexture::unbind()
{
    WDOpenGLSystem::sharedOpenGLSystem()->textureManager.unbindTexture(Id);
}

#pragma mark -

/// --------------------------------------------------------------------------------------------------------
bool WDTexture::createTexture()
{
    if (Id != 0) return false;
    
    Id = WDOpenGLSystem::sharedOpenGLSystem()->textureManager.createTexture();
    bind();
    WDOpenGLSystem::sharedOpenGLSystem()->textureManager.textureSetClampToEdge(Id);
    WDOpenGLSystem::sharedOpenGLSystem()->textureManager.textureSetLinearFiltering(Id);
    
    return true;
}

/// --------------------------------------------------------------------------------------------------------
void WDTexture::deleteTexture()
{
    if (Id == 0) return;
    WDOpenGLSystem::sharedOpenGLSystem()->textureManager.deleteTexture(Id);
    Id = 0;

    state = WDTextureNotInited;
}

#pragma mark - ctor/dtor

/// --------------------------------------------------------------------------------------------------------
WDTexture::WDTexture()
{
    Id = 0;
    size = CGSizeZero;
    state = WDTextureNotInited;
    usingMipmaps = false;
    
    createTexture();
}

/// --------------------------------------------------------------------------------------------------------
WDTexture::WDTexture(CGSize _size) : WDTexture()
{
    this->size = _size;
    WDOpenGLSystem::sharedOpenGLSystem()->textureManager.textureLoadFromMemory(this, nullptr);
}

/// --------------------------------------------------------------------------------------------------------
WDTexture::~WDTexture()
{
    deleteTexture();
}

