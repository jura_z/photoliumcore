//
//  WDTexture.h
//  PhotoliumCore
//
//  Created by Yurii Zakipnyi on 10/9/12.
//  Copyright (c) 2012 Yurii Zakipnyi. All rights reserved.
//

typedef enum
{
    WDTextureNotInited = 0,
    WDTextureLoading,
    WDTextureLoaded,
} WDTextureState;


class WDTexture
{
protected:
    GLuint Id;
    CGSize size;
    WDTextureState state;
    bool usingMipmaps;
protected:
    bool createTexture();
    void deleteTexture();
public:
    WDTexture();
    WDTexture(CGSize _size);
    virtual ~WDTexture();

    GLuint getId() const { return  Id; }
    bool isUsingMipmaps() const { return usingMipmaps; }
    CGSize getSize() const { return size; }
    
    void bind();
    void bind(int indx);
    void unbind();
};