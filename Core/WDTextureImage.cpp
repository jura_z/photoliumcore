//
//  WDTextureImage.m
//  PhotoliumCore
//
//  Created by Yurii Zakipnyi on 10/9/12.
//  Copyright (c) 2012 Yurii Zakipnyi. All rights reserved.
//

#import "WDTextureImage.h"
#import "WDTexture.h"

#include "WDOpenGLSystem.h"

/// --------------------------------------------------------------------------------------------------------
WDTextureImage::WDTextureImage(CGImageRef newImageSource, bool mipMaps/* = false */) : WDTexture()
{
    if (newImageSource == nil)
    {
        NSLog(@"ERROR [WDTexture] CGImageRef == nil");
        return;
    }
    
    if (state != WDTextureNotInited)
    {
        NSLog(@"ERROR [WDTexture] You are not allowed to load this texture");
        return;
    }
    
    usingMipmaps = mipMaps;
    
    WDOpenGLSystem::sharedOpenGLSystem()->textureManager.textureSetLinearFiltering(Id);
    
    state = WDTextureLoading;
    //dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^
   {
       CGFloat widthOfImage = CGImageGetWidth(newImageSource);
       CGFloat heightOfImage = CGImageGetHeight(newImageSource);
       size = CGSizeMake(widthOfImage, heightOfImage);
       CGSize pixelSizeToUseForTexture = size;
       
       BOOL shouldRedrawUsingCoreGraphics = true;
       
       // For now, deal with images larger than the maximum texture size by resizing to be within that limit
       CGSize scaledImageSizeToFitOnGPU = WDOpenGLSystem::sharedOpenGLSystem()->textureManager.sizeThatFitsWithinATextureForSize(size);
       if (!CGSizeEqualToSize(scaledImageSizeToFitOnGPU, size))
       {
           size = scaledImageSizeToFitOnGPU;
           pixelSizeToUseForTexture = size;
           shouldRedrawUsingCoreGraphics = true;
       }
       
       if (usingMipmaps)
       {
           // In order to use mipmaps, you need to provide power-of-two textures, so convert to the next largest power of two and stretch to fill
           CGFloat powerClosestToWidth = ceilf(log2f(size.width));
           CGFloat powerClosestToHeight = ceilf(log2f(size.height));
           
           pixelSizeToUseForTexture = CGSizeMake(powf(2.0, powerClosestToWidth), powf(2.0, powerClosestToHeight));
           
           shouldRedrawUsingCoreGraphics = true;
       }
       
       GLubyte *imageData = nullptr;
       __block CFDataRef dataFromImageDataProvider = nullptr;
       
       //    CFAbsoluteTime elapsedTime, startTime = CFAbsoluteTimeGetCurrent();
       
       if (shouldRedrawUsingCoreGraphics)
       {
           int width = (int)pixelSizeToUseForTexture.width;
           int height = (int)pixelSizeToUseForTexture.height;
           
           // For resized image, redraw
           imageData = (GLubyte *) calloc(1, width * height * 4);
           
           CGColorSpaceRef genericRGBColorspace = CGColorSpaceCreateDeviceRGB();
           
           
           CGContextRef imgcontext = CGBitmapContextCreate( imageData, width, height, 8, 4 * width, genericRGBColorspace, kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedFirst );
           
           // flip
           CGContextTranslateCTM(imgcontext, 0.0, height);
           CGContextScaleCTM(imgcontext, 1.0, -1.0);
           
           CGContextSetBlendMode(imgcontext, kCGBlendModeCopy);
           
           CGContextDrawImage( imgcontext, CGRectMake( 0, 0, width, height ), newImageSource );
           
           
           CGContextRelease(imgcontext);
           
           CGColorSpaceRelease(genericRGBColorspace);
       }
       else
       {
           // Access the raw image bytes directly
           dataFromImageDataProvider = CGDataProviderCopyData(CGImageGetDataProvider(newImageSource));
           imageData = (GLubyte *)CFDataGetBytePtr(dataFromImageDataProvider);
       }
       
       //dispatch_async(dispatch_get_main_queue(), ^
                      {
                          size = pixelSizeToUseForTexture;
                          bind();
                          WDOpenGLSystem::sharedOpenGLSystem()->textureManager.textureLoadFromMemory(this, imageData, true);
                          
                          state = WDTextureLoaded;
                          NSLog(@"[WDTexture] Texture loaded");
                          
                          if (shouldRedrawUsingCoreGraphics)
                              free(imageData);
                          else
                              CFRelease(dataFromImageDataProvider);
                          
                          WDOpenGLSystem::sharedOpenGLSystem()->textureManager.unbindTexture();
                      }//);
   }//);
}

/// --------------------------------------------------------------------------------------------------------
WDTextureImage::WDTextureImage(GLubyte *data, CGSize size, GLint format, bool mipMaps/* = false */) : WDTexture()
{
    bind();
    
    this->size = size;
    
    WDOpenGLSystem::sharedOpenGLSystem()->textureManager.textureLoadFromMemory(this, data, format == GL_BGRA);
    
    state = WDTextureLoaded;
    
    WDOpenGLSystem::sharedOpenGLSystem()->textureManager.unbindTexture();
}
