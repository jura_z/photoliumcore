//
//  WDTextureImage.h
//  PhotoliumCore
//
//  Created by Yurii Zakipnyi on 10/9/12.
//  Copyright (c) 2012 Yurii Zakipnyi. All rights reserved.
//

#import "WDTexture.h"

class WDTextureImage : public WDTexture
{
public:
    WDTextureImage(CGImageRef imgSource, bool mipMaps = false);
    WDTextureImage(GLubyte *data, CGSize size, GLint format, bool mipMaps = false);
};
