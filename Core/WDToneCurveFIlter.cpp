//
//  WDToneCurveFilter.m
//  PhotoliumCore
//
//  Created by Yurii Zakipnyi on 10/13/12.
//  Copyright (c) 2012 Yurii Zakipnyi. All rights reserved.
//

#import "WDToneCurveFIlter.h"

#import "WDTextureImage.h"
#import "GLProgram.h"

//  GPUImageACVFile
//
//  ACV File format Parser
//  Please refer to http://www.adobe.com/devnet-apps/photoshop/fileformatashtml/PhotoshopFileFormats.htm#50577411_pgfId-1056330
//

@interface GPUImageACVFile : NSObject
{
    short version;
    short totalCurves;
    
    NSArray *rgbCompositeCurvePoints;
    NSArray *redCurvePoints;
    NSArray *greenCurvePoints;
    NSArray *blueCurvePoints;
}

@property(strong,nonatomic) NSArray *rgbCompositeCurvePoints;
@property(strong,nonatomic) NSArray *redCurvePoints;
@property(strong,nonatomic) NSArray *greenCurvePoints;
@property(strong,nonatomic) NSArray *blueCurvePoints;

- (id) initWithCurveFile:(NSString*)curveFile;

@end

@implementation GPUImageACVFile

@synthesize rgbCompositeCurvePoints, redCurvePoints, greenCurvePoints, blueCurvePoints;

- (id) initWithCurveFile:(NSString*)curveFile
{
    self = [super init];
	if (self != nil)
	{
        NSString *bundleCurvePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent: curveFile];
        
        NSFileHandle* file = [NSFileHandle fileHandleForReadingAtPath: bundleCurvePath];
        
        if (file == nil)
        {
            NSLog(@"Failed to open file");
            
            return self;
        }
        
        NSData *databuffer;
        
        // 2 bytes, Version ( = 1 or = 4)
        databuffer = [file readDataOfLength: 2];
        version = CFSwapInt16BigToHost(*(int*)([databuffer bytes]));
        
        // 2 bytes, Count of curves in the file.
        [file seekToFileOffset:2];
        databuffer = [file readDataOfLength:2];
        totalCurves = CFSwapInt16BigToHost(*(int*)([databuffer bytes]));
        
        NSMutableArray *curves = [NSMutableArray array];
        
        float pointRate = (1.0f / 255.0f);
        // The following is the data for each curve specified by count above
        for (NSInteger x = 0; x<totalCurves; x++)
        {
            // 2 bytes, Count of points in the curve (short integer from 2...19)
            databuffer = [file readDataOfLength:2];
            short pointCount = CFSwapInt16BigToHost(*(int*)([databuffer bytes]));
            
            NSMutableArray *points = [NSMutableArray array];
            // point count * 4
            // Curve points. Each curve point is a pair of short integers where
            // the first number is the output value (vertical coordinate on the
            // Curves dialog graph) and the second is the input value. All coordinates have range 0 to 255.
            for (NSInteger y = 0; y<pointCount; y++)
            {
                databuffer = [file readDataOfLength:2];
                short yy = CFSwapInt16BigToHost(*(int*)([databuffer bytes]));
                databuffer = [file readDataOfLength:2];
                short xx = CFSwapInt16BigToHost(*(int*)([databuffer bytes]));
                
                [points addObject:[NSValue valueWithCGSize:CGSizeMake(xx * pointRate, yy * pointRate)]];
            }
            
            [curves addObject:points];
        }
        
        [file closeFile];
        
        rgbCompositeCurvePoints = [curves objectAtIndex:0];
        redCurvePoints = [curves objectAtIndex:1];
        greenCurvePoints = [curves objectAtIndex:2];
        blueCurvePoints = [curves objectAtIndex:3];
	}
	
	return self;
    
}

@end




CGSize WDToneCurveFilter::processWithSize()
{
    return CGSizeMake(640, 480);
}

bool WDToneCurveFilter::setupShader()
{
    /// --------------------------------------------------------------------------------------------------------
    NSString *const VS_ToneCurve = SHADER_STRING
    (
     attribute vec4 inputTextureCoordinate;
     attribute vec4 position;
     
     varying vec2 textureCoordinate;
     
     void main()
     {
         gl_Position = position;
         textureCoordinate = inputTextureCoordinate.xy;
     }
     );
    
    /// --------------------------------------------------------------------------------------------------------
    NSString *const FS_ToneCurve = SHADER_STRING
    (
     varying highp vec2 textureCoordinate;
     uniform sampler2D inputImageTexture;
     uniform sampler2D toneCurveTexture;
     
     void main()
     {
         lowp vec4 textureColor = texture2D(inputImageTexture, textureCoordinate);
         textureColor.r = texture2D(toneCurveTexture, vec2(textureColor.r, 0.0)).r * textureColor.a;
         textureColor.g = texture2D(toneCurveTexture, vec2(textureColor.g, 0.0)).g * textureColor.a;
         textureColor.b = texture2D(toneCurveTexture, vec2(textureColor.b, 0.0)).b * textureColor.a;
         
         lowp vec2 vTexCoord = textureCoordinate - 0.5;
         mediump float vignette  = 1.0 - dot(vTexCoord, vTexCoord);
         textureColor.xyz *= clamp(pow(vignette, 4.0) + 0.3, 0.0, 1.0);
         
         gl_FragColor = textureColor;
     }
     );
    /// --------------------------------------------------------------------------------------------------------
    
    defProgram = new GLProgram([VS_ToneCurve UTF8String], [FS_ToneCurve UTF8String]);
    
    if (!defProgram->initialized)
    {
        defProgram->addAttribute("inputTextureCoordinate");
        defProgram->addAttribute("position");
        
        if (!defProgram->link())
        {
            assert(false);//NSAssert(false, @"Filter shader link failed");
            return false;
        }
        
        addInput("inputImageTexture");
        addInput("toneCurveTexture");
    }

    toneTexture = createToneCurveTexture();
    connectTexture(toneTexture, "toneCurveTexture");
    
    return true;
}

WDToneCurveFilter::WDToneCurveFilter(const std::string &curveFileName, bool useVignette)
{
    GPUImageACVFile *curve = [[GPUImageACVFile alloc] initWithCurveFile:[NSString stringWithUTF8String:curveFileName.c_str()]];
    
    setRgbCompositeControlPoints(curve.rgbCompositeCurvePoints);
    setRedControlPoints(curve.redCurvePoints);
    setGreenControlPoints(curve.greenCurvePoints);
    setBlueControlPoints(curve.blueCurvePoints);
}

WDToneCurveFilter::~WDToneCurveFilter()
{
    if (toneTexture)
    {
        delete toneTexture;
        toneTexture = nullptr;
    }
}


WDTextureImage *WDToneCurveFilter::createToneCurveTexture()
{
    WDTextureImage *result = nullptr;

    GLubyte *toneCurveByteArray = (GLubyte *)calloc(256 * 4, sizeof(GLubyte));
    if ( ([redCurve count] >= 256) && ([greenCurve count] >= 256) && ([blueCurve count] >= 256) && ([rgbCompositeCurve count] >= 256))
    {
        for (unsigned int currentCurveIndex = 0; currentCurveIndex < 256; currentCurveIndex++)
        {
            // BGRA for upload to texture
            toneCurveByteArray[currentCurveIndex * 4] = fmax(currentCurveIndex + [[blueCurve objectAtIndex:currentCurveIndex] floatValue] + [[rgbCompositeCurve objectAtIndex:currentCurveIndex] floatValue], 0);
            toneCurveByteArray[currentCurveIndex * 4 + 1] = fmax(currentCurveIndex + [[greenCurve objectAtIndex:currentCurveIndex] floatValue] + [[rgbCompositeCurve objectAtIndex:currentCurveIndex] floatValue], 0);
            toneCurveByteArray[currentCurveIndex * 4 + 2] = fmax(currentCurveIndex + [[redCurve objectAtIndex:currentCurveIndex] floatValue] + [[rgbCompositeCurve objectAtIndex:currentCurveIndex] floatValue], 0);
            toneCurveByteArray[currentCurveIndex * 4 + 3] = 255;
        }
        
        result = new WDTextureImage(toneCurveByteArray, CGSizeMake(256, 1), GL_BGRA);
        
    } else
        NSLog(@"updateToneCurveTexture incomplete data");

    free(toneCurveByteArray);
    return result;
}

#pragma mark -
#pragma mark Curve calculation

NSMutableArray *WDToneCurveFilter::getPreparedSplineCurve(NSArray *points)
{
    if (points && [points count] > 0)
    {
        // Sort the array.
        NSArray *sortedPoints = [points sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
            float x1 = [(NSValue *)a CGPointValue].x;
            float x2 = [(NSValue *)b CGPointValue].x;
            if (x1 < x2) return NSOrderedAscending;
            if (x1 > x2) return NSOrderedDescending;
            return NSOrderedSame;
        }];
        
        // Convert from (0, 1) to (0, 255).
        NSMutableArray *convertedPoints = [NSMutableArray arrayWithCapacity:[sortedPoints count]];
        for (uint32_t i=0; i<[points count]; i++){
            CGPoint point = [[sortedPoints objectAtIndex:i] CGPointValue];
            point.x = point.x * 255;
            point.y = point.y * 255;
            
            [convertedPoints addObject:[NSValue valueWithCGPoint:point]];
        }
        
        
        NSMutableArray *splinePoints = this->splineCurve(convertedPoints);
        
        // If we have a first point like (0.3, 0) we'll be missing some points at the beginning
        // that should be 0.
        CGPoint firstSplinePoint = [[splinePoints objectAtIndex:0] CGPointValue];
        
        if (firstSplinePoint.x > 0) {
            for (int i=firstSplinePoint.x; i >= 0; i--) {
                CGPoint newCGPoint = CGPointMake(i, 0);
                [splinePoints insertObject:[NSValue valueWithCGPoint:newCGPoint] atIndex:0];
            }
        }
        
        // Insert points similarly at the end, if necessary.
        CGPoint lastSplinePoint = [[splinePoints objectAtIndex:([splinePoints count] - 1)] CGPointValue];
        if (lastSplinePoint.x < 255) {
            for (int i = lastSplinePoint.x + 1; i <= 255; i++) {
                CGPoint newCGPoint = CGPointMake(i, 255);
                [splinePoints addObject:[NSValue valueWithCGPoint:newCGPoint]];
            }
        }
        
        // Prepare the spline points.
        NSMutableArray *preparedSplinePoints = [NSMutableArray arrayWithCapacity:[splinePoints count]];
        for (uint32_t i=0; i<[splinePoints count]; i++)
        {
            CGPoint newPoint = [[splinePoints objectAtIndex:i] CGPointValue];
            CGPoint origPoint = CGPointMake(newPoint.x, newPoint.x);
            
            float distance = sqrt(pow((origPoint.x - newPoint.x), 2.0) + pow((origPoint.y - newPoint.y), 2.0));
            
            if (origPoint.y > newPoint.y)
            {
                distance = -distance;
            }
            
            [preparedSplinePoints addObject:[NSNumber numberWithFloat:distance]];
        }
        
        return preparedSplinePoints;
    }
    
    return nil;
}


NSMutableArray *WDToneCurveFilter::splineCurve(NSArray *points)
{
    NSMutableArray *sdA = secondDerivative(points);
    
    // Is [points count] equal to [sdA count]?
    //    int n = [points count];
    NSUInteger n = [sdA count];
    double sd[n];
    
    // From NSMutableArray to sd[n];
    for (NSUInteger i=0; i<n; i++)
    {
        sd[i] = [[sdA objectAtIndex:i] doubleValue];
    }
    
    
    NSMutableArray *output = [NSMutableArray arrayWithCapacity:(n+1)];
    
    for(NSUInteger i=0; i<n-1 ; i++)
    {
        CGPoint cur = [[points objectAtIndex:i] CGPointValue];
        CGPoint next = [[points objectAtIndex:(i+1)] CGPointValue];
        
        for(int x=cur.x;x<(int)next.x;x++)
        {
            double t = (double)(x-cur.x)/(next.x-cur.x);
            
            double a = 1-t;
            double b = t;
            double h = next.x-cur.x;
            
            double y= a*cur.y + b*next.y + (h*h/6)*( (a*a*a-a)*sd[i]+ (b*b*b-b)*sd[i+1] );
            
            if (y > 255.0)
            {
                y = 255.0;
            }
            else if (y < 0.0)
            {
                y = 0.0;
            }
            
            [output addObject:[NSValue valueWithCGPoint:CGPointMake(x, y)]];
        }
    }
    
    // If the last point is (255, 255) it doesn't get added.
    if ([output count] == 255) {
        [output addObject:[points lastObject]];
    }
    return output;
}

NSMutableArray *WDToneCurveFilter::secondDerivative(NSArray *cgPoints)
{
    int n = (int)[cgPoints count];
    if ((n <= 0) || (n == 1))
    {
        return nil;
    }
    
    double matrix[n][3];
    double result[n];
    matrix[0][1]=1;
    // What about matrix[0][1] and matrix[0][0]? Assuming 0 for now (Brad L.)
    matrix[0][0]=0;
    matrix[0][2]=0;
    
    for(int i=1;i<n-1;i++)
    {
        CGPoint P1 = [[cgPoints objectAtIndex:(i-1)] CGPointValue];
        CGPoint P2 = [[cgPoints objectAtIndex:i] CGPointValue];
        CGPoint P3 = [[cgPoints objectAtIndex:(i+1)] CGPointValue];
        
        matrix[i][0]=(double)(P2.x-P1.x)/6;
        matrix[i][1]=(double)(P3.x-P1.x)/3;
        matrix[i][2]=(double)(P3.x-P2.x)/6;
        result[i]=(double)(P3.y-P2.y)/(P3.x-P2.x) - (double)(P2.y-P1.y)/(P2.x-P1.x);
    }
    
    // What about result[0] and result[n-1]? Assuming 0 for now (Brad L.)
    result[0] = 0;
    result[n-1] = 0;
	
    matrix[n-1][1]=1;
    // What about matrix[n-1][0] and matrix[n-1][2]? For now, assuming they are 0 (Brad L.)
    matrix[n-1][0]=0;
    matrix[n-1][2]=0;
    
  	// solving pass1 (up->down)
  	for(int i=1;i<n;i++)
    {
		double k = matrix[i][0]/matrix[i-1][1];
		matrix[i][1] -= k*matrix[i-1][2];
		matrix[i][0] = 0;
		result[i] -= k*result[i-1];
    }
	// solving pass2 (down->up)
	for(int i=n-2;i>=0;i--)
    {
		double k = matrix[i][2]/matrix[i+1][1];
		matrix[i][1] -= k*matrix[i+1][0];
		matrix[i][2] = 0;
		result[i] -= k*result[i+1];
	}
    
    double y2[n];
    for(int i=0;i<n;i++) y2[i]=result[i]/matrix[i][1];
    
    NSMutableArray *output = [NSMutableArray arrayWithCapacity:n];
    for (int i=0;i<n;i++)
    {
        [output addObject:[NSNumber numberWithDouble:y2[i]]];
    }
    
    return output;
}



#pragma mark -
#pragma mark Accessors

void WDToneCurveFilter::setRGBControlPoints(NSArray *points)
{
    redControlPoints = [points copy];
    redCurve = getPreparedSplineCurve(redControlPoints);
    
    greenControlPoints = [points copy];
    greenCurve = getPreparedSplineCurve(greenControlPoints);
    
    blueControlPoints = [points copy];
    blueCurve = getPreparedSplineCurve(blueControlPoints);
}


void WDToneCurveFilter::setRgbCompositeControlPoints(NSArray *newValue)
{
    rgbCompositeControlPoints = [newValue copy];
    rgbCompositeCurve = getPreparedSplineCurve(rgbCompositeControlPoints);
}


void WDToneCurveFilter::setRedControlPoints(NSArray *newValue)
{
    redControlPoints = [newValue copy];
    redCurve = getPreparedSplineCurve(redControlPoints);
}


void WDToneCurveFilter::setGreenControlPoints(NSArray *newValue)
{
    greenControlPoints = [newValue copy];
    greenCurve = getPreparedSplineCurve(greenControlPoints);
}


void WDToneCurveFilter::setBlueControlPoints(NSArray *newValue)
{
    blueControlPoints = [newValue copy];
    blueCurve = getPreparedSplineCurve(blueControlPoints);
}
