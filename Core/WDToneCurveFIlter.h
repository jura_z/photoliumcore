//
//  WDToneCurveFilter.h
//  PhotoliumCore
//
//  Created by Yurii Zakipnyi on 10/13/12.
//  Copyright (c) 2012 Yurii Zakipnyi. All rights reserved.
//

#include "WDFilter.h"

class WDTextureImage;

class WDToneCurveFilter : public WDFilter
{
protected:
    WDTexture *toneTexture;
    NSArray *redCurve, *greenCurve, *blueCurve, *rgbCompositeCurve;

protected:
    void setRgbCompositeControlPoints(NSArray *newValue);
    void setRedControlPoints(NSArray *newValue);
    void setGreenControlPoints(NSArray *newValue);
    void setBlueControlPoints(NSArray *newValue);

public:
    NSArray *redControlPoints;
    NSArray *greenControlPoints;
    NSArray *blueControlPoints;
    NSArray *rgbCompositeControlPoints;

    WDTextureImage *createToneCurveTexture();

    NSMutableArray *getPreparedSplineCurve(NSArray *points);
    NSMutableArray *splineCurve(NSArray *points);
    NSMutableArray *secondDerivative(NSArray *cgPoints);

public:
    WDToneCurveFilter(const std::string &curveFileName, bool useVignette);
    ~WDToneCurveFilter();
    
    bool setupShader();

    CGSize processWithSize();

    void setRGBControlPoints(NSArray *points);
};
