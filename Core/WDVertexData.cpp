//
//  WDVertexData.cpp
//  PhotoliumCore
//
//  Created by Yurii Zakipnyi on 10/8/12.
//  Copyright (c) 2012 Yurii Zakipnyi. All rights reserved.
//

#import "WDVertexData.h"

WDVertexData *WDVertexData::fullScreen()
{
    static WDVertexData *instance = nullptr;
    if (instance == nullptr)
        instance = new WDVertexData(0);
    return instance;
}

WDVertexData *WDVertexData::fullScreenCamera()
{
    static WDVertexData *instance = nullptr;
    if (instance == nullptr)
        instance = new WDVertexData(1);
    return instance;
}

const GLfloat *WDVertexData::textureCoordinatesForOrientation(int orient)
{
    static const GLfloat noRotationTextureCoordinates[] =
    {
        0.0f, 1.0f,
        0.0f, 0.0f,
        1.0f, 1.0f,
        1.0f, 0.0f,
    };
    

    static const GLfloat noRotationTextureCoordinatesCamera[] =
    {
        0.0f, 1.0f,
        1.0f, 1.0f,
        0.0f, 0.0f,
        1.0f, 0.0f,
    };

    switch (orient)
    {
        case 1:
            return noRotationTextureCoordinatesCamera;

        case 0:
        default:
            return noRotationTextureCoordinates;
    }
}

/// --------------------------------------------------------------------------------------------------------
void WDVertexData::draw(GLuint vertexLoc, GLuint texLoc)
{
    static BOOL setup = false;
    
    if (setup == false)
    {
        glEnableVertexAttribArray(vertexLoc);
        glEnableVertexAttribArray(texLoc);
        
        glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
        glVertexAttribPointer(vertexLoc, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
        
        glBindBuffer(GL_ARRAY_BUFFER, texBuffer);
        glVertexAttribPointer(texLoc, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
        setup = true;
    }
    
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
}

#pragma mark ctor/dtor

/// --------------------------------------------------------------------------------------------------------
WDVertexData::WDVertexData(int orientType)
{
    static const GLfloat imageVertices[] = {
        -1.0f,  1.0f,
        -1.0f, -1.0f,
        1.0f,  1.0f,
        1.0f, -1.0f,
    };

    otype = orientType;

    const GLfloat *texData = WDVertexData::textureCoordinatesForOrientation(otype);
    
    glGenBuffers(1, &texBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, texBuffer);
    glBufferData(GL_ARRAY_BUFFER, 8 * sizeof(float), &texData[0], GL_STATIC_DRAW);

    glGenBuffers(1, &vertexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
    glBufferData(GL_ARRAY_BUFFER, 8 * sizeof(float), &imageVertices[0], GL_STATIC_DRAW);
}

/// --------------------------------------------------------------------------------------------------------
WDVertexData::~WDVertexData()
{
    if (texBuffer)
        glDeleteBuffers(1, &texBuffer), texBuffer = 0;
    if (vertexBuffer)
        glDeleteBuffers(1, &vertexBuffer), vertexBuffer = 0;
}
