//
//  WDVertexData.h
//  PhotoliumCore
//
//  Created by Yurii Zakipnyi on 10/8/12.
//  Copyright (c) 2012 Yurii Zakipnyi. All rights reserved.
//

class WDVertexData
{
    GLuint texBuffer;
    GLuint vertexBuffer;
    int otype;
protected:
    const GLfloat *textureCoordinatesForOrientation(int orient);
public:
    static WDVertexData *fullScreen();
    static WDVertexData *fullScreenCamera();

    WDVertexData(int orientType);
    ~WDVertexData();

    void draw(GLuint vertexLoc, GLuint texLoc);
};