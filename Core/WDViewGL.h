//
//  WDViewGL.h
//  PhotoliumCore
//
//  Created by Yurii Zakipnyi on 10/8/12.
//  Copyright (c) 2012 Yurii Zakipnyi. All rights reserved.
//

@interface WDViewGL : UIView

@property (nonatomic, retain) EAGLContext *context;


- (void)changeFilter;
- (void)toggleBlur;

@end
