//
//  WDViewGL.m
//  PhotoliumCore
//
//  Created by Yurii Zakipnyi on 10/8/12.
//  Copyright (c) 2012 Yurii Zakipnyi. All rights reserved.
//

#import "WDViewGL.h"
#import <QuartzCore/QuartzCore.h>

#import "WDFilter.h"
#import "WDResult.h"
#import "WDBoxBlurFilter.h"
#import "WDToneCurveFIlter.h"

#include "WDTexture.h"

//#import "WDCamera.h"

#import "WDTextureImage.h"
#import "WDFBOSystem.h"
#import "WDFBObject.h"

#import "WDFilterSystem.h"

#import "WDProfiler.h"
#include "WDPerfTimer.h"

#include "WDOpenGLSystem.h"

@implementation WDViewGL
{
    WDFBObject *defaultFramebuffer;
    
    CADisplayLink* displayLink;

    WDTextureImage *tex;

//    WDCamera *cam;


    WDFilter *filter1;
    WDFilter *filter2_1;
    WDFilter *filter2_2;
    WDFilter *filter2_3;
    WDFilter *filter2_4;
    WDResult *result;
}

@synthesize context;

/// --------------------------------------------------------------------------------------------------------
+ (Class)layerClass
{
    return [CAEAGLLayer class];
}

#pragma mark - Init OpenGL

/// --------------------------------------------------------------------------------------------------------
- (void)initializeOpenGL
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stopRenderLoop) name:UIApplicationWillTerminateNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stopRenderLoop) name:UIApplicationWillResignActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(startRenderLoop) name:UIApplicationDidBecomeActiveNotification object:nil];
    
    CAEAGLLayer *eaglLayer = (CAEAGLLayer *)self.layer;
    
    eaglLayer.opaque = TRUE;
    eaglLayer.drawableProperties = @{ kEAGLDrawablePropertyRetainedBacking : [NSNumber numberWithBool:FALSE],
                                      kEAGLDrawablePropertyColorFormat : kEAGLColorFormatRGBA8 };
    
    context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
    
    if (!context || ![EAGLContext setCurrentContext:context])
    {
        NSLog(@"Could not create context!");
        return;
    }

    defaultFramebuffer = new WDFBObject();
    defaultFramebuffer->attachGeneralRendertarget((CAEAGLLayer *)self.layer, context);
    
    NSLog(@"InitializeOpenGL.. done");
    
    glEnable(GL_CULL_FACE);
    
    WDOpenGLSystem::sharedOpenGLSystem()->textureManager.maximumTextureSizeForThisDevice();
    WDOpenGLSystem::sharedOpenGLSystem()->textureManager.maximumTextureUnitsForThisDevice();
    
    [[WDProfiler sharedProfiler] printStatus];
    
    if ([[UIApplication sharedApplication] applicationState] == UIApplicationStateActive)
        [self startRenderLoop];

    WDPerfTimer *p = [WDPerfTimer perfTimerWithName:@"Loading"];
    
    tex = new WDTextureImage([UIImage imageNamed:@"Basic.png"].CGImage);

    filter2_1 = new WDToneCurveFilter("02.acv", true);
    filter2_1->setupShader();
    
    filter2_2 = new WDToneCurveFilter("06.acv", true);
    filter2_2->setupShader();
    
    filter2_3 = new WDToneCurveFilter("aqua.acv", true);
    filter2_3->setupShader();
    
    filter2_4 = new WDToneCurveFilter("crossprocess.acv", true);
    filter2_4->setupShader();

    filter1 = new WDBoxBlurFilter();
    filter1->setupShader();
    
    
    
    result = new WDResult(defaultFramebuffer);
    result->setupShader();
    
    [p printElapsedTimeAndResetWithMessage:@"O1"];

//    cam = [[WDCamera alloc] initWithSessionPreset:AVCaptureSessionPreset640x480 cameraPosition:AVCaptureDevicePositionBack context:context];
//    [cam startCameraCapture];

    filter2_1->connectTexture(tex, "inputImageTexture");
    filter2_1->addTarget(result, "inputImageTexture");
    
    WDFilterSystem::sharedFilters()->reconfigure();

    glClearColor(1.0f, 0.3f, 0.3f, 1.0f);

    [p printElapsedTimeAndResetWithMessage:@"O2"];
}

#pragma mark - Draw

/// --------------------------------------------------------------------------------------------------------
- (void)drawFrame
{
    if (context == nil)
    {
        NSLog(@"Context not set!");
        return;
    }

    //make it the current context for rendering
    [EAGLContext setCurrentContext:context];

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    WDFilterSystem::sharedFilters()->process();
    
    [context presentRenderbuffer:GL_RENDERBUFFER];
}



#pragma mark - Display link

/// --------------------------------------------------------------------------------------------------------
- (void)startRenderLoop
{
    if(displayLink) return;

    NSLog(@"startRenderLoop");
    displayLink = [self.window.screen displayLinkWithTarget:self selector:@selector(drawFrame)];
    [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
}

/// --------------------------------------------------------------------------------------------------------
- (void)stopRenderLoop
{
    if (displayLink == nil) return;

    [displayLink invalidate];
    displayLink = nil;
    NSLog(@"stopRenderLoop");
}



#pragma mark - UIView

/// --------------------------------------------------------------------------------------------------------
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self initializeOpenGL];
    }
    return self;
}

/// --------------------------------------------------------------------------------------------------------
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        [self initializeOpenGL];
    }
    return self;
}

/// --------------------------------------------------------------------------------------------------------
//As soon as the view is resized or new subviews are added, this method is called,
//apparently the framebuffers are invalid in this case so we delete them
//and have them recreated the next time we draw to them
- (void)layoutSubviews
{
//    [defaultFramebuffer deleteFBO:<#(CGSize)#>];
}

/// --------------------------------------------------------------------------------------------------------
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    if (tex)
    {
        delete tex;
        tex = nullptr;
    }
    delete filter2_1;
    delete filter2_2;
    delete filter2_3;
    delete filter2_4;
    delete filter1;
    delete result;
    delete defaultFramebuffer;
}




static int i = 1;
static bool tblur = false;

- (void)changeFilter
{
    i++;
    if (i > 4) i = 1;
    
    WDFilter *f = filter2_1;
    
    if (i == 2) f = filter2_2;
    if (i == 3) f = filter2_3;
    if (i == 4) f = filter2_4;
    
    f->connectTexture(tex, "inputImageTexture");
    
    if (tblur)
    {
        f->addTarget(filter1, "inputImageTexture");
        filter1->addTarget(result, "inputImageTexture");
    }
    else
    {
        f->addTarget(result, "inputImageTexture");
    }
    
    WDFilterSystem::sharedFilters()->reconfigure();
}

- (void)toggleBlur
{
    tblur = !tblur;
    
    WDFilter *f = filter2_1;
    
    if (i == 2) f = filter2_2;
    if (i == 3) f = filter2_3;
    if (i == 4) f = filter2_4;

    if (tblur)
    {
        f->addTarget(filter1, "inputImageTexture");
        filter1->addTarget(result, "inputImageTexture");
    }
    else
    {
        f->removeTraget(result, "inputImageTexture");
        f->addTarget(result, "inputImageTexture");
    }

    WDFilterSystem::sharedFilters()->reconfigure();
}



@end
