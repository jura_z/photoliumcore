//
//  WDOpenGLFBOManager.cpp
//  PhotoliumCore
//
//  Created by Yurii Zakipnyi on 1/24/13.
//  Copyright (c) 2013 Yurii Zakipnyi. All rights reserved.
//

#include "WDOpenGLFBOManager.h"
#include "WDTexture.h"

/// --------------------------------------------------------------------------------------------------------
void WDOpenGLFBOManager::checkFBO()
{
    GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    if (status != GL_FRAMEBUFFER_COMPLETE)
    {
        NSLog(@"Incomplete filter FBO: %d", status);
        assert(false);
    }
}

/// --------------------------------------------------------------------------------------------------------
GLuint WDOpenGLFBOManager::createFBO()
{
    GLuint fboId = 0;
    glGenFramebuffers(1, &fboId);
    return fboId;
}

/// --------------------------------------------------------------------------------------------------------
void WDOpenGLFBOManager::deleteFBO(GLuint fboId)
{
    if (fboId)
    {
        glDeleteFramebuffers(1, &fboId);
    }
}

/// --------------------------------------------------------------------------------------------------------
void WDOpenGLFBOManager::bindFBO(GLuint fboId)
{
    if (currentFboId != fboId)
    {
        currentFboId = fboId;
        glBindFramebuffer(GL_FRAMEBUFFER, currentFboId);
        
    }
}


/// --------------------------------------------------------------------------------------------------------
GLuint WDOpenGLFBOManager::createRenderbuffer()
{
    GLuint fboId = 0;
    glGenRenderbuffers(1, &fboId);
    return fboId;
}

/// --------------------------------------------------------------------------------------------------------
void WDOpenGLFBOManager::bindRenderbuffer(GLuint renderbufferId)
{
    glBindRenderbuffer(GL_RENDERBUFFER, renderbufferId);
}

/// --------------------------------------------------------------------------------------------------------
void WDOpenGLFBOManager::deleteRenderbuffer(GLuint renderbufferId)
{
    glDeleteRenderbuffers(1, &renderbufferId);
}

/// --------------------------------------------------------------------------------------------------------
void WDOpenGLFBOManager::getCurrentRenderbufferSize(GLint *width, GLint *height)
{
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, height);
}


/// --------------------------------------------------------------------------------------------------------
void WDOpenGLFBOManager::attachRenderbuffer(GLuint renderbufferId)
{
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, renderbufferId);
    checkFBO();
}

/// --------------------------------------------------------------------------------------------------------
void WDOpenGLFBOManager::attachTexture(WDTexture *texture)
{
    texture->bind();
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texture->getId(), 0);
    texture->unbind();
    checkFBO();
}
