//
//  WDOpenGLFBOManager.h
//  PhotoliumCore
//
//  Created by Yurii Zakipnyi on 1/24/13.
//  Copyright (c) 2013 Yurii Zakipnyi. All rights reserved.
//

class WDFBObject;
class WDTexture;

class WDOpenGLFBOManager
{

    GLuint currentFboId;
public:
    void checkFBO();
    
    GLuint createFBO();
    void deleteFBO(GLuint fboId);
    
    GLuint createRenderbuffer();
    void bindRenderbuffer(GLuint renderbufferId);
    void deleteRenderbuffer(GLuint renderbufferId);

    void getCurrentRenderbufferSize(GLint *width, GLint *height);
    
    void attachRenderbuffer(GLuint renderbufferId);
    void attachTexture(WDTexture *texture);
    
    void bindFBO(GLuint fboId);
};