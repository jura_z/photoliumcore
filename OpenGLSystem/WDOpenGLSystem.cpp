//
//  WDOpenGLSystem.cpp
//  PhotoliumCore
//
//  Created by Yurii Zakipnyi on 1/9/13.
//  Copyright (c) 2013 Yurii Zakipnyi. All rights reserved.
//

#include "WDOpenGLSystem.h"
#include "WDTexture.h"

WDOpenGLSystem *WDOpenGLSystem::sharedOpenGLSystem()
{
    static WDOpenGLSystem *instance = nullptr;
    
    static std::once_flag flag;
    std::call_once(flag, [](){ instance = new WDOpenGLSystem(); });
    return instance;
}

void WDOpenGLSystem::setViewport(int width, int height)
{
    if (currentViewportX != width || currentViewportY != height)
    {
        currentViewportX = width;
        currentViewportY = height;
        glViewport(0, 0, currentViewportX, currentViewportY);
    }
}
