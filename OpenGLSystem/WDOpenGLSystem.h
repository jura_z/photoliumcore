//
//  WDOpenGLSystem.h
//  PhotoliumCore
//
//  Created by Yurii Zakipnyi on 1/9/13.
//  Copyright (c) 2013 Yurii Zakipnyi. All rights reserved.
//

#pragma once

#include "WDOpenGLTextureManager.h"
#include "WDOpenGLFBOManager.h"

class WDOpenGLSystem
{
    WDOpenGLSystem() {};
    
    WDOpenGLSystem &operator= (const WDOpenGLSystem &other);
    WDOpenGLSystem(const WDOpenGLSystem& other);
    ~WDOpenGLSystem();
    
private:
    int currentViewportX = 0;
    int currentViewportY = 0;
public:
    static WDOpenGLSystem *sharedOpenGLSystem();
    
    void setViewport(int width, int height);
public:
    WDOpenGLTextureManager  textureManager;
    WDOpenGLFBOManager      fboManager;
};