//
//  WDOpenGLTextureManager.cpp
//  PhotoliumCore
//
//  Created by Yurii Zakipnyi on 1/24/13.
//  Copyright (c) 2013 Yurii Zakipnyi. All rights reserved.
//

#include "WDOpenGLTextureManager.h"
#include "WDTexture.h"
#include "WDProfiler.h"

/// --------------------------------------------------------------------------------------------------------
int WDOpenGLTextureManager::getFreeTextureSlot()
{
    for (int i = 0; i < this->maximumTextureUnitsForThisDevice(); ++i)
    {
        if (texSlots[i] == 0)
            return i;
    }
    NSLog(@"free texture slot not found.");
    return 0;
}

/// --------------------------------------------------------------------------------------------------------
void WDOpenGLTextureManager::bindTexture(GLuint texID)
{
    this->bindTexture(texID, activeSlot);
}

/// --------------------------------------------------------------------------------------------------------
void WDOpenGLTextureManager::pushCurrentTextureSlotState()
{
    textureStack.push( std::make_pair(currentTextureSlot(), currentTextureSlotIndex()) );
}

/// --------------------------------------------------------------------------------------------------------
void WDOpenGLTextureManager::popCurrentTextureSlotState()
{
    textureSlotInfo state = textureStack.top();
    textureStack.pop();
    bindTexture(state.first, state.second);
}

/// --------------------------------------------------------------------------------------------------------
void WDOpenGLTextureManager::bindTexture(GLuint texID, uint32_t slotIndx)
{
    if (texSlots[slotIndx] != texID)
    {
        texSlots[slotIndx] = texID;
        
        if (activeSlot != slotIndx)
        {
//            NSLog(@"set activeSlot: %d", slotIndx);
            glActiveTexture(GL_TEXTURE0 + slotIndx);
            activeSlot = slotIndx;
        }
        
//        NSLog(@"set tex [%d] %d", slotIndx, texID);
        glBindTexture(GL_TEXTURE_2D, texID);
    }
}

/// --------------------------------------------------------------------------------------------------------
void WDOpenGLTextureManager::unbindTexture()
{
    this->unbindTextureInSlot(activeSlot);
}

/// --------------------------------------------------------------------------------------------------------
void WDOpenGLTextureManager::unbindTexture(GLuint texID)
{
    for (int i = 0; i < maximumTextureUnitsForThisDevice(); ++i)
        if (texSlots[i] == texID)
        {
            unbindTextureInSlot(i);
        }
}

/// --------------------------------------------------------------------------------------------------------
void WDOpenGLTextureManager::unbindTextureInSlot(int slotIndx)
{
    this->bindTexture(0, slotIndx);
}


/// --------------------------------------------------------------------------------------------------------
GLint WDOpenGLTextureManager::maximumTextureSizeForThisDevice()
{
    static GLint maxTextureSize = 0;
    
    static std::once_flag flag;
    std::call_once(flag, [](){ glGetIntegerv(GL_MAX_TEXTURE_SIZE, &maxTextureSize); });
    return maxTextureSize;
}

/// --------------------------------------------------------------------------------------------------------
GLuint WDOpenGLTextureManager::createTexture()
{
    GLuint textureId;
    glGenTextures(1, &textureId);
    return textureId;
}

/// --------------------------------------------------------------------------------------------------------
void WDOpenGLTextureManager::deleteTexture(GLuint textureId)
{
    glDeleteTextures(1, &textureId);
}

/// --------------------------------------------------------------------------------------------------------
void WDOpenGLTextureManager::textureSetClampToEdge(GLuint textureId)
{
    pushCurrentTextureSlotState();
    bindTexture(textureId);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    popCurrentTextureSlotState();
}

/// --------------------------------------------------------------------------------------------------------
void WDOpenGLTextureManager::textureSetLinearFiltering(GLuint textureId, bool mipmap)
{
    pushCurrentTextureSlotState();
    bindTexture(textureId);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, (mipmap) ? GL_LINEAR_MIPMAP_LINEAR : GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    popCurrentTextureSlotState();
}

/// --------------------------------------------------------------------------------------------------------
void WDOpenGLTextureManager::textureSetPointFiltering(GLuint textureId)
{
    pushCurrentTextureSlotState();
    bindTexture(textureId);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    popCurrentTextureSlotState();
}

/// --------------------------------------------------------------------------------------------------------
GLint WDOpenGLTextureManager::maximumTextureUnitsForThisDevice()
{
    static GLint maxTextureUnits = 0;
    
    static std::once_flag flag;
    std::call_once(flag, [](){ glGetIntegerv(GL_MAX_TEXTURE_IMAGE_UNITS, &maxTextureUnits); });
    return maxTextureUnits;

}

/// --------------------------------------------------------------------------------------------------------
CGSize WDOpenGLTextureManager::sizeThatFitsWithinATextureForSize(CGSize inputSize)
{
    GLint maxTextureSize = this->maximumTextureSizeForThisDevice();
    if ( (inputSize.width < maxTextureSize) && (inputSize.height < maxTextureSize) )
    {
        return inputSize;
    }
    
    CGSize adjustedSize;
    if (inputSize.width > inputSize.height)
    {
        adjustedSize.width = (CGFloat)maxTextureSize;
        adjustedSize.height = ((CGFloat)maxTextureSize / inputSize.width) * inputSize.height;
    }
    else
    {
        adjustedSize.height = (CGFloat)maxTextureSize;
        adjustedSize.width = ((CGFloat)maxTextureSize / inputSize.height) * inputSize.width;
    }
    
    return adjustedSize;
}

/// --------------------------------------------------------------------------------------------------------
void WDOpenGLTextureManager::textureLoadFromMemory(WDTexture *tex, void *ptr, bool bgra/* = false */)
{
    [[WDProfiler sharedProfiler] printStatus];
    
    pushCurrentTextureSlotState();
    tex->bind();
    
    if (tex->isUsingMipmaps())
    {
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    }
    
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, (int)tex->getSize().width, (int)tex->getSize().height, 0, (bgra) ? GL_BGRA : GL_RGBA, GL_UNSIGNED_BYTE, ptr);

    if (tex->isUsingMipmaps())
    {
        glGenerateMipmap(GL_TEXTURE_2D);
    }
    
    popCurrentTextureSlotState();
    
    [[WDProfiler sharedProfiler] printStatus];
}


/// --------------------------------------------------------------------------------------------------------
WDOpenGLTextureManager::WDOpenGLTextureManager()
{
    // warm-up
    maximumTextureUnitsForThisDevice();
    maximumTextureSizeForThisDevice();
}
