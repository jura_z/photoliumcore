//
//  WDOpenGLTextureManager.h
//  PhotoliumCore
//
//  Created by Yurii Zakipnyi on 1/24/13.
//  Copyright (c) 2013 Yurii Zakipnyi. All rights reserved.
//

#include <stack>

class WDTexture;

class WDOpenGLTextureManager
{
    typedef std::pair<GLuint, int> textureSlotInfo; // id, index

    WDOpenGLTextureManager &operator= (const WDOpenGLTextureManager &other);
    WDOpenGLTextureManager(const WDOpenGLTextureManager& other);
protected:
    void pushCurrentTextureSlotState();
    void popCurrentTextureSlotState();
protected:
    GLuint texSlots[32] = {0};
    uint32_t activeSlot = 0;
    
    std::stack<textureSlotInfo> textureStack;
public:

    WDOpenGLTextureManager();

    int currentTextureSlotIndex() const { return activeSlot; }
    GLuint currentTextureSlot() const { return texSlots[activeSlot]; }
    
    int getFreeTextureSlot();
    void bindTexture(GLuint texID);
    void bindTexture(GLuint texID, uint32_t slotIndx);
    void unbindTexture();
    void unbindTexture(GLuint texID);
    void unbindTextureInSlot(int slotIndx);
    
    GLuint createTexture();
    void deleteTexture(GLuint textureId);
    
    void textureSetClampToEdge(GLuint textureId);
    void textureSetLinearFiltering(GLuint textureId, bool mipmap = false);
    void textureSetPointFiltering(GLuint textureId);

    void textureLoadFromMemory(WDTexture *tex, void *ptr, bool bgra = false);
    
    GLint maximumTextureSizeForThisDevice();
    GLint maximumTextureUnitsForThisDevice();
    CGSize sizeThatFitsWithinATextureForSize(CGSize inputSize);

};
