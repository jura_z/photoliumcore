//
//  WDAppDelegate.h
//  PhotoliumCore
//
//  Created by Yurii Zakipnyi on 10/8/12.
//  Copyright (c) 2012 Yurii Zakipnyi. All rights reserved.
//

#import <UIKit/UIKit.h>

@class WDViewController;

@interface WDAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) WDViewController *viewController;

@end
