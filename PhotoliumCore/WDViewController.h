//
//  WDViewController.h
//  PhotoliumCore
//
//  Created by Yurii Zakipnyi on 10/8/12.
//  Copyright (c) 2012 Yurii Zakipnyi. All rights reserved.
//

#import <UIKit/UIKit.h>

@class WDViewGL;

@interface WDViewController : UIViewController

@property (retain, nonatomic) IBOutlet WDViewGL *viewGL;

@end
