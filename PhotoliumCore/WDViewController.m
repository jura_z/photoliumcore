//
//  WDViewController.m
//  PhotoliumCore
//
//  Created by Yurii Zakipnyi on 10/8/12.
//  Copyright (c) 2012 Yurii Zakipnyi. All rights reserved.
//

#import "WDViewController.h"
#import "WDViewGL.h"

@interface WDViewController ()

@end

@implementation WDViewController

@synthesize viewGL;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)filterAction:(id)sender
{
    [viewGL changeFilter];
}

- (IBAction)blurAction:(id)sender
{
    [viewGL toggleBlur];
}

- (void)viewDidUnload {
    [self setViewGL:nil];
    [super viewDidUnload];
}
@end
